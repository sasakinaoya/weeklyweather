package weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class Weather3HourEntity implements Serializable{

    @SerializedName("city")
    public City city;

    public double message;

    public  class City implements Serializable{
        @SerializedName("coord")
        public Coord coord;
        public class Coord implements Serializable{
            public double lat;
            public double lon;
        }

        public String country;
        public int id;
        public String name;
        public int population;

    }

    public int cnt;
    public int cod;

    @SerializedName("list")
    public List<WeatherList> list;

    public class WeatherList implements Serializable{
        @SerializedName("clouds")
        public Clouds clouds;
        public class Clouds implements Serializable{
            public int all;
        }
        public long dt;
        public String dt_txt;
        @SerializedName("main")
        public Main main;
        public class Main implements Serializable{
            public double grnd_level;
            public double humidity;
            public double pressure;
            public double sea_level;
            public double temp;
            public double temp_kf;
            public double temp_max;
            public double temp_min;
        }

        @SerializedName("sys")
        public Sys sys;

        public class Sys implements Serializable{
            public String pod;
        }

        @SerializedName("weather")
        public List<Weather> weather;

        public class Weather implements Serializable{
            public String description;
            public String icon;
            public int id;
            public String main;
        }

        @SerializedName("wind")
        public Wind wind;

        public class Wind implements Serializable{
            public double deg;
            public double spped;
        }
    }
}
