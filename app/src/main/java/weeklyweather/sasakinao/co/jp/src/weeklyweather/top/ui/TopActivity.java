package weeklyweather.sasakinao.co.jp.src.weeklyweather.top.ui;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.squareup.otto.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import weeklyweather.sasakinao.co.jp.src.WeeklyWeatherApplication;
import weeklyweather.sasakinao.co.jp.src.common.util.LatitudeLongitude;
import weeklyweather.sasakinao.co.jp.src.model.location.LocationGetter;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.BusHolder;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.WeatherApiAccessor;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.WeatherApiParameter;
import weeklyweather.sasakinao.co.jp.weeklyweather.R;

public class TopActivity extends FragmentActivity {

    @Inject
    WeatherApiAccessor weatherAccessor;

    @Inject
    LocationGetter locationGetter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((WeeklyWeatherApplication) getApplication()).inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                if(locationGetter.isFetching() || weatherAccessor.isFetching()){
                    return super.onOptionsItemSelected(item);
                }
                setContentView(new ProgressBar(this));
                locationGetter.fetchLocation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        BusHolder.get().register(this);
        setContentView(new ProgressBar(this));
        locationGetter.fetchLocation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Subscriberの登録を解除する
        BusHolder.get().unregister(this);
    }

    @Subscribe
    public void onFetchedLocation(LocationGetter.Event event){
        if(event.isVaild()){
//            Toast.makeText(this, " location"+event.location.getLatitude()+",lon:"+event.location.getLongitude(), Toast.LENGTH_LONG).show();
            List list3Hour = WeatherApiParameter.get3HourOpenWeatherApiParam(new LatitudeLongitude(event.location.getLatitude(), event.location.getLongitude()));
            List listCurent = WeatherApiParameter.getCurrentOpenWeatherApiParam(new LatitudeLongitude(event.location.getLatitude(), event.location.getLongitude()));
            weatherAccessor.fetchWeatherApiBackground(new AQuery(this), list3Hour, listCurent);

        } else {
            Toast.makeText(this, "Error. Please reboot this app", Toast.LENGTH_LONG).show();
        }
    }


    @Subscribe
    public void onFetchedWeather(WeatherApiAccessor.Event event) {
        if (event.isVaild()) {
            setContentView(R.layout.activity_top);
            ButterKnife.inject(this);

            FragmentTransaction ftCurrent = getFragmentManager().beginTransaction();
            ftCurrent.replace(R.id.card_current_weather, CurrentWeatherFragment.getInstance(event.weatherCurrentEntity))
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null)
                    .commit();

            FragmentTransaction ftWeek = getFragmentManager().beginTransaction();
            ftWeek.replace(R.id.card_week_weather, WeekWeatherFragment.getInstance(event.weather5DaysEntity, event.weather3Hourentity))
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null)
                    .commit();

        } else {
            Toast.makeText(this, "Error. Please reboot this app", Toast.LENGTH_LONG).show();
        }
    }

}
