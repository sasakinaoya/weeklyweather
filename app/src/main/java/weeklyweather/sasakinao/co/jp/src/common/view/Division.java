package weeklyweather.sasakinao.co.jp.src.common.view;

import android.graphics.Color;

public class Division {

    private float y;
    private int color;
    private int strokeWidth;
    private String text;
    private int textSize;
    private int textColor;

    public Division() {
        this.textColor = Color.RED;
        this.color = Color.RED;
        this.strokeWidth = 2;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(int strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
