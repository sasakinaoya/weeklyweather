package weeklyweather.sasakinao.co.jp.src.common.util;

public class Position {
    private int y;
    private int x;

    public Position(int x, int y){
        this.y = y;
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }
}
