package weeklyweather.sasakinao.co.jp.src.common.util;

import android.os.AsyncTask;

public abstract class SimpleBackgroundTask extends AsyncTask<Void,Void,Void>{
    @Override
    protected Void doInBackground(Void... param) {
        doInBackground();
        return null;
    }

    protected abstract void doInBackground();
}
