package weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WeatherCurrentEntity implements Serializable{

    public String base;
    @SerializedName("clouds")
    public Clouds clouds;
    public class Clouds implements Serializable{
        public int all;
    }
    public int cod;
    @SerializedName("coord")
    public Coord coord;
    public class Coord implements Serializable{
        public double lat;
        public double lon;
    }
    public long dt;
    public int id;
    public Main main;
    public class Main implements Serializable{
        public double pressure;
        public double humidity;
        public double sea_level;
        public double temp;
        public double temp_max;
        public double temp_min;
    }
    public String name;

    @SerializedName("sys")
    public Sys sys;

    public class Sys implements Serializable{
        public String pod;
        public String country;
        public int id;

    }


    @SerializedName("weather")
    public List<Weather> weather;

    public class Weather implements Serializable{
        public String description;
        public String icon;
        public int id;
        public String main;
    }

    @SerializedName("wind")
    public Wind wind;

    public class Wind implements Serializable{
        public double deg;
        public double spped;
    }

}
