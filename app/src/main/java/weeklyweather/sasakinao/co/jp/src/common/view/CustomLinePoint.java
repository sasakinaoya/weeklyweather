package weeklyweather.sasakinao.co.jp.src.common.view;

import com.echo.holographlibrary.LinePoint;

public class CustomLinePoint extends LinePoint{

    String string;

    public CustomLinePoint(float x, float y) {
        super(x,y);
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}
