package weeklyweather.sasakinao.co.jp.src.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class WeatherUtil {

    public static double getCelsiusFromKelvin(double delvin){
        return delvin - 273.15;
    }

    public static String getKelvinTempString(double kelvin){
        double before = getCelsiusFromKelvin(kelvin);
        String after = String.format("%.1f℃", before);
        return after;
    }

    public static String getDateStringFromUnixTime(long unixTime){
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd");
        String str = sdf.format(new Date(unixTime * 1000));
        return str;
    }

    public static String getTimeStringFromUnixTime(long unixTime){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String str = sdf.format(new Date(unixTime * 1000));
        return str;
    }


    public static Calendar getMonthDay(long unixTime){
        Calendar datetime = Calendar.getInstance();
        Date date = new Date(unixTime * 1000);
        datetime.setTime(date);
//        Logger.e("chek","date?","date.getD:"+date.getDay()+ " Month:"+date.getMonth());
        return datetime;
    }

}
