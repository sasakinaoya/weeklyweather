package weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Weather5DaysEntity implements Serializable{
    @SerializedName("city")
    public City city;

    public double message;

    public class City implements Serializable {
        @SerializedName("coord")
        public Coord coord;

        public class Coord implements Serializable {
            public double lat;
            public double lon;
        }

        public String country;
        public int id;
        public String name;
        public int population;

    }

    public int cnt;
    public int cod;

    @SerializedName("list")
    public List<WeatherList> list;

    public class WeatherList implements Serializable {
        public int clouds;
        public int deg;
        public long dt;
        public int humidity;
        public double pressure;
        public double speed;
        @SerializedName("temp")
        public Temp temp;

        public class Temp implements Serializable {
            public double day;
            public double eve;
            public double max;
            public double min;
            public double morn;
            public double night;
        }

        @SerializedName("weather")
        public List<Weather> weather;

        public class Weather implements Serializable {
            public String description;
            public String icon;
            public int id;
            public String main;
        }
    }

}
