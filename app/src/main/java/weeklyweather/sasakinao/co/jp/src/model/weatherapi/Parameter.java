package weeklyweather.sasakinao.co.jp.src.model.weatherapi;

public abstract class Parameter {
    private String value;
    public Parameter(String value) {
        this.value = value;
    }

    public abstract String getName();

    public String getValue() {
        return value;
    }
}
