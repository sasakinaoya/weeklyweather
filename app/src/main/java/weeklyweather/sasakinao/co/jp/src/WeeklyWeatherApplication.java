package weeklyweather.sasakinao.co.jp.src;

import android.app.Application;

import dagger.ObjectGraph;

public class WeeklyWeatherApplication extends Application {

    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        objectGraph = ObjectGraph.create(getModules());
    }

    protected WeeklyWeatherApplicationModule getModules() {
        return new WeeklyWeatherApplicationModule(this);
    }

    public <T> T inject(T obj) {
        return objectGraph.inject(obj);
    }

}
