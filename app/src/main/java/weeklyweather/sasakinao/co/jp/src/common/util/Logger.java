package weeklyweather.sasakinao.co.jp.src.common.util;

import android.util.Log;

public class Logger {

    public static void v(Object object, String title, String message){
        Log.v(object.getClass().getCanonicalName() + "#" + title, message);
    }

    public static void i(Object object, String title, String message){
        Log.i(object.getClass().getCanonicalName() + "#" + title, message);
    }

    public static void e(Object object, String title, String message){
        Log.e(object.getClass().getCanonicalName() + "#" + title, message);
    }

}
