package weeklyweather.sasakinao.co.jp.src.model.weatherapi;

import android.os.Handler;

import com.squareup.otto.Bus;

public final class BusHolder {
    private static Bus sBus;
    private static Handler handl = null;

    public static Bus get() {
        if(sBus == null){
            sBus = new Bus();
            handl = new Handler();
        }
        return sBus;
    }

    public static void postMain(final Object event){
        handl.post(new Runnable() {
            @Override
            public void run() {
                sBus.post(event);
            }
        });
    }
}