package weeklyweather.sasakinao.co.jp.src.model.weatherapi;

import android.content.Context;
import android.net.Uri;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.Weather3HourEntity;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.Weather5DaysEntity;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.WeatherCurrentEntity;
import weeklyweather.sasakinao.co.jp.src.common.util.Logger;
import weeklyweather.sasakinao.co.jp.src.common.util.SimpleBackgroundTask;
import weeklyweather.sasakinao.co.jp.weeklyweather.R;

public class WeatherApiAccessor {
    private final long CACHE_TIME = 1000 * 60 * 15;

    private final String weather5Days = "/api.openweathermap.org/data/2.5/forecast/daily";
    private final String weather3HourUrl = "/api.openweathermap.org/data/2.5/forecast";
    private final String weatherCurrentUrl = "/api.openweathermap.org/data/2.5/weather";

    private boolean isFetching;

    public class Event{

        public Event(Weather3HourEntity weather3Hourentity, WeatherCurrentEntity weatherCurrentEntity, Weather5DaysEntity weather5DaysEntity) {
            this.weather3Hourentity = weather3Hourentity;
            this.weatherCurrentEntity = weatherCurrentEntity;
            this.weather5DaysEntity  = weather5DaysEntity;
        }

        public Weather5DaysEntity weather5DaysEntity;
        public Weather3HourEntity weather3Hourentity;
        public WeatherCurrentEntity weatherCurrentEntity;

        public boolean isVaild() {
            return (weather3Hourentity != null) && (weatherCurrentEntity != null ) && (weather5DaysEntity != null);
        }
    }

    public void fetchWeatherApiBackground(final AQuery aq, final List<Parameter> params3Hour, final List<Parameter> paramsCurrent){

        isFetching = true;
        new SimpleBackgroundTask() {
            @Override
            protected void doInBackground() {
                AjaxCallback<JSONObject> acCurrent = new AjaxCallback<JSONObject>();
                acCurrent.url(getCurrentApiUrl(aq.getContext(), paramsCurrent));
                Logger.v(this,"current","url:"+acCurrent.getUrl());
                acCurrent.expire(CACHE_TIME);
                acCurrent.type(JSONObject.class);
                aq.sync(acCurrent);
                WeatherCurrentEntity entityCurrent = null;
                if(acCurrent.getResult() != null){
                    entityCurrent = new Gson().fromJson(acCurrent.getResult().toString(),WeatherCurrentEntity.class);
                }

                AjaxCallback<JSONObject> ac3Hour = new AjaxCallback<JSONObject>();
                ac3Hour.url(get3HourApiUrl(aq.getContext(), params3Hour));
                Logger.v(this,"3Hour","url:"+ac3Hour.getUrl());
                ac3Hour.expire(CACHE_TIME);
                ac3Hour.type(JSONObject.class);
                aq.sync(ac3Hour);
                Weather3HourEntity entity3Hour = null;
                if(ac3Hour.getResult() != null){
                    entity3Hour = new Gson().fromJson(ac3Hour.getResult().toString(),Weather3HourEntity.class);
                }

                AjaxCallback<JSONObject> ac5Days = new AjaxCallback<JSONObject>();
                ac5Days.url(get5DaysApiUrl(aq.getContext(), params3Hour));
                Logger.v(this,"5days","url:"+ac5Days.getUrl());
                ac5Days.expire(CACHE_TIME);
                ac5Days.type(JSONObject.class);
                aq.sync(ac5Days);
                Weather5DaysEntity weather5DaysEntity = null;
                if(ac5Days.getResult() != null){
                    weather5DaysEntity = new Gson().fromJson(ac5Days.getResult().toString(),Weather5DaysEntity.class);
                }


                isFetching = false;
                BusHolder.postMain(new Event(entity3Hour,entityCurrent,weather5DaysEntity));
            }

        }.execute();
    }

    private String get5DaysApiUrl(Context context, List<Parameter> params3Hour) {
        String apiKey = context.getResources().getString(R.string.open_weather_map_api_key);
        Uri.Builder builder = new Uri.Builder()
                .scheme("http")
                .appendEncodedPath(weather5Days)
                .appendQueryParameter("APPID", apiKey);
        for (Parameter param : params3Hour) {
            builder.appendQueryParameter(param.getName(), param.getValue());
        }

        return builder.toString();
    }

    private String getCurrentApiUrl(Context context, List<Parameter> paramsCurrent) {
        String apiKey = context.getResources().getString(R.string.open_weather_map_api_key);
        Uri.Builder builder = new Uri.Builder()
                .scheme("http")
                .appendEncodedPath(weatherCurrentUrl)
                .appendQueryParameter("APPID", apiKey);
        for (Parameter param : paramsCurrent) {
            builder.appendQueryParameter(param.getName(), param.getValue());
        }

        return builder.toString();
    }

    private String get3HourApiUrl(Context context, List<Parameter> params3Hour) {
        String apiKey = context.getResources().getString(R.string.open_weather_map_api_key);
        Uri.Builder builder = new Uri.Builder()
                .scheme("http")
                .appendEncodedPath(weather3HourUrl)
                .appendQueryParameter("APPID", apiKey);
        for (Parameter param : params3Hour) {
            builder.appendQueryParameter(param.getName(), param.getValue());
        }

        return builder.toString();
    }

    public static String weatherIconUrl(String url){
        return "http://openweathermap.org/img/w/"+url+".png";
    }

    public boolean isFetching() {
        return isFetching;
    }

    public void setFetching(boolean isFetching) {
        this.isFetching = isFetching;
    }



}
