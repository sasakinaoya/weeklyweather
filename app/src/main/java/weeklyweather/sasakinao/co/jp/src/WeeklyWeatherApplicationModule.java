package weeklyweather.sasakinao.co.jp.src;

import android.content.Context;

import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.WeatherflipModule;

@Module(
        includes = {
                WeatherflipModule.class,

        },
        injects = {
                WeeklyWeatherApplication.class
        }
)
public final class WeeklyWeatherApplicationModule {
    private final Context app;

    public WeeklyWeatherApplicationModule(Context app) {
        this.app = app;
    }

    @Provides
    Context provideContext() { return app; }


    @Provides
    Picasso providePicaso(){
        return new Picasso.Builder(app).build();
    }

}

