package weeklyweather.sasakinao.co.jp.src.model.weatherapi;

import java.util.ArrayList;
import java.util.List;

import weeklyweather.sasakinao.co.jp.src.common.util.LatitudeLongitude;

public class WeatherApiParameter {

    public static List<Parameter> get5And16DayOpenWeatherApiParam(LatitudeLongitude latlon, int count){
        List list = new ArrayList();
        list.add(new Latitude(latlon.getLatitude()));
        list.add(new Longitude(latlon.getLongitude()));
        list.add(new Count(count));
        list.add(new Mode());
        return list;
    }

    public static List<Parameter> getCurrentOpenWeatherApiParam(LatitudeLongitude latlon){
        List list = new ArrayList();
        list.add(new Latitude(latlon.getLatitude()));
        list.add(new Longitude(latlon.getLongitude()));
        list.add(new Mode());
        return list;
    }

    public static List<Parameter> get3HourOpenWeatherApiParam(LatitudeLongitude latlon){
        List list = new ArrayList();
        list.add(new Latitude(latlon.getLatitude()));
        list.add(new Longitude(latlon.getLongitude()));
        list.add(new Count(5));
        list.add(new Mode());
        return list;
    }


    public static class Latitude extends Parameter {
        public Latitude(double lat){
            super(String.valueOf(lat));
        }

        @Override
        public String getName() {
            return "lat";
        }
    }

    public static class Longitude extends Parameter {
        public Longitude(double lon){
            super(String.valueOf(lon));
        }

        @Override
        public String getName() {
            return "lon";
        }
    }

    public static class Count extends Parameter{

        public Count(int count) {
            super(String.valueOf(count));
        }

        @Override
        public String getName() {
            return "cnt";
        }
    }

    public static class Mode extends Parameter{
        public Mode(){
            super("json");
        }

        @Override
        public String getName() {
            return "mode";
        }
    }

}
