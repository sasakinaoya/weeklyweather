package weeklyweather.sasakinao.co.jp.src.weeklyweather.top.component;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.widget.FrameLayout;

import com.echo.holographlibrary.Line;
import com.echo.holographlibrary.LinePoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import weeklyweather.sasakinao.co.jp.src.common.util.WeatherUtil;
import weeklyweather.sasakinao.co.jp.src.common.view.CastomLineGraph;
import weeklyweather.sasakinao.co.jp.src.common.view.Division;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.Weather3HourEntity;
import weeklyweather.sasakinao.co.jp.weeklyweather.R;

public class GraphLineView extends FrameLayout {
    private static int DIVIDER_MAX_NUMBER = 3;

    private Weather3HourEntity weather3Hour;
    private SparseArray<Weather3HourEntity.WeatherList> sameDayList;

    public GraphLineView(Context context) {
        super(context);
    }

    public GraphLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void init(Weather3HourEntity weather3Hour) {
        this.weather3Hour = weather3Hour;
    }

    public void startGraphAnimation(long dt) {
        sameDayList = createSameDayList(dt);
//        Logger.e(this, "startGraph", "size:" + sameDayList.size());
        initializeGraph();

    }

    private SparseArray<Weather3HourEntity.WeatherList> createSameDayList(long dt) {
        SparseArray<Weather3HourEntity.WeatherList> list = new SparseArray();
        Calendar calender = WeatherUtil.getMonthDay(dt);
        int counter = 0;
        for (Weather3HourEntity.WeatherList weather3HourItem : weather3Hour.list) {
            Calendar tempCalender = WeatherUtil.getMonthDay(weather3HourItem.dt);

            if (calender.get(Calendar.MONTH) == tempCalender.get(Calendar.MONTH)) {
                if (calender.get(Calendar.DATE) == tempCalender.get(Calendar.DATE)) {
                    list.put(counter++, weather3HourItem);
                }
            }
        }
        return list;
    }

    private void initializeGraph() {
        CastomLineGraph li = (CastomLineGraph) this.findViewById(R.id.line_view);
        li.removeAllLines();

        if (sameDayList.size() <= 0) {
            this.findViewById(R.id.empty_text).setVisibility(View.VISIBLE);
            return;
        } else {
            this.findViewById(R.id.empty_text).setVisibility(View.GONE);
        }
        //tempLine
        Line l = new Line();
        l.setColor(getResources().getColor(R.color.orange));
        l.setShowingPoints(true);

        int max = (int) WeatherUtil.getCelsiusFromKelvin(sameDayList.get(0).main.temp);
        int min = max;
        List<String> pointTexts = new ArrayList<String>();
        for (int i = 0; i < sameDayList.size(); i++) {
            String pointText = WeatherUtil.getTimeStringFromUnixTime(sameDayList.get(i).dt);
            pointTexts.add(pointText);

            double temp = WeatherUtil.getCelsiusFromKelvin(sameDayList.get(i).main.temp);
            addLinePoint(l, temp, i, sameDayList.size(), sameDayList.get(i).dt, R.color.red);
            if (max < temp) {
                max = (int) temp;
            } else if (min > temp) {
                min = (int) temp;
            }
        }

        List<Division> divisions = new ArrayList<Division>();
        int divideNumber;
        for(int i = 2;;i += 2){
            int tempMin = (min - (min % i));
            int tempMax = (max + (max % i));
            if((tempMax - tempMin) / i <= DIVIDER_MAX_NUMBER){
                divideNumber = i;
                break;
            }
        }
        int temp = min - min % divideNumber;
        for(;;){
            Division division = new Division();
            division.setColor(getResources().getColor(R.color.red));
            division.setStrokeWidth(1);
            division.setTextSize((int) getResources().getDimension(R.dimen.default_textSize));
            division.setY(temp);
            division.setText(temp+"℃");
            divisions.add(division);
            if(temp > max){
                break;
            } else {
                temp += divideNumber;
            }
        }
        li.setDivisions(divisions);

        li.setPointTexts(pointTexts);
        li.addLine(l);
        li.setDividerTextSize(getResources().getDimension(R.dimen.graph_devider_text_size));
        li.setRangeY(divisions.get(0).getY() - 5, divisions.get(divisions.size()-1).getY() + 1);
        li.setRangeX(-20, 110);

        //クリックイベント
        li.setOnPointClickedListener(new CastomLineGraph.OnPointClickedListener() {

            @Override
            public void onClick(int lineIndex, int pointIndex) {
                //TODO click Event
//                Toast.makeText(getContext(),
//                        "Line " + lineIndex + " / Point " + pointIndex + " clicked",
//                        Toast.LENGTH_SHORT)
//                        .show();
            }
        });

    }

    private void addLinePoint(Line line, double temp, int position, int maxSize, long dt, int color) {
        LinePoint p = new LinePoint();
        if(maxSize==1){
            maxSize=2;
        }
        int x = ((100 / (maxSize - 1)) * position);
        p.setX(x);
        p.setY(temp);
        p.setColor(color);
        p.setSelectedColor(getResources().getColor(R.color.transparent_blue));

        line.addPoint(p);

    }
}
