package weeklyweather.sasakinao.co.jp.src.weeklyweather;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import weeklyweather.sasakinao.co.jp.src.model.location.LocationGetter;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.WeatherApiAccessor;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.top.component.WeekTabStrip;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.top.component.WeekWeatherLayoutView;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.top.ui.CurrentWeatherFragment;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.top.ui.OneDayWeatherFragment;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.top.ui.TopActivity;

@Module(
        injects = {
                TopActivity.class,
                OneDayWeatherFragment.class,
                CurrentWeatherFragment.class,
                WeekWeatherLayoutView.class,
                WeekTabStrip.class
        },
        complete = false,
        library = true
)
public class WeatherflipModule {

    @Provides
    public WeatherApiAccessor provideWeatherAccessor(){
        return new WeatherApiAccessor();
    }

    @Provides
    public LocationGetter providelocationGetter(Context context){
        return new LocationGetter(context);
    }
}