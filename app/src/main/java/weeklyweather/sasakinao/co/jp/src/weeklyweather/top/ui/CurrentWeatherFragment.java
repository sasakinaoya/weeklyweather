package weeklyweather.sasakinao.co.jp.src.weeklyweather.top.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import weeklyweather.sasakinao.co.jp.src.WeeklyWeatherApplication;
import weeklyweather.sasakinao.co.jp.src.common.util.WeatherUtil;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.WeatherApiAccessor;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.WeatherCurrentEntity;
import weeklyweather.sasakinao.co.jp.weeklyweather.R;

public class CurrentWeatherFragment extends Fragment {

    @InjectView(R.id.city_name)
    TextView cityName;

    @InjectView(R.id.month)
    TextView month;

    @InjectView(R.id.time)
    TextView time;

    @InjectView(R.id.temperature)
    TextView temperature;


    @InjectView(R.id.wind_speed)
    TextView wind;

    @InjectView(R.id.hurmity)
    TextView humidity;

    @InjectView(R.id.image_weathwe)
    ImageView weatherImage;

    @Inject
    Picasso picaso;

    private WeatherCurrentEntity weatherCurrent;

    public static CurrentWeatherFragment getInstance(WeatherCurrentEntity weatherList) {
        CurrentWeatherFragment fragment = new CurrentWeatherFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("curent_weather", weatherList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_current_weather, container, false);
        ButterKnife.inject(this, layout);
        ((WeeklyWeatherApplication) getActivity().getApplication()).inject(this);
        weatherCurrent = (WeatherCurrentEntity) getArguments().getSerializable("curent_weather");

        cityName.setText("("+weatherCurrent.name+")");

        month.setText(WeatherUtil.getDateStringFromUnixTime(weatherCurrent.dt));
        time.setText(WeatherUtil.getTimeStringFromUnixTime(weatherCurrent.dt));

        temperature.setText(WeatherUtil.getKelvinTempString(weatherCurrent.main.temp));
        wind.setText(String.format("%.1f", weatherCurrent.wind.spped)+"m/s (wind)");
        humidity.setText(String.format("%.1f", weatherCurrent.main.humidity)+"%"+" (humid)");
        picaso.load(WeatherApiAccessor.weatherIconUrl(weatherCurrent.weather.get(0).icon))
                .into(weatherImage);
        return layout;
    }

}
