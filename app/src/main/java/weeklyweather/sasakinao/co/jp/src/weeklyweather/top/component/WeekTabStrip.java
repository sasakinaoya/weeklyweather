package weeklyweather.sasakinao.co.jp.src.weeklyweather.top.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import weeklyweather.sasakinao.co.jp.src.WeeklyWeatherApplication;
import weeklyweather.sasakinao.co.jp.src.common.util.WeatherUtil;
import weeklyweather.sasakinao.co.jp.src.common.view.PagerSlidingTabStrip;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.WeatherApiAccessor;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.Weather5DaysEntity;
import weeklyweather.sasakinao.co.jp.weeklyweather.R;

public class WeekTabStrip extends PagerSlidingTabStrip{

    @Inject
    Picasso picasso;

    @InjectView(R.id.date)
    TextView date;
    @InjectView(R.id.temp_max)
    TextView tempMax;

    @InjectView(R.id.temp_min)
    TextView tempMin;

    @InjectView(R.id.image_weather)
    ImageView imageWeather;

    private List<Weather5DaysEntity.WeatherList> list;

    public WeekTabStrip(Context context) {
        super(context);
        ((WeeklyWeatherApplication) context.getApplicationContext()).inject(this);
    }

    public WeekTabStrip(Context context, AttributeSet attrs) {
        super(context, attrs);
        ((WeeklyWeatherApplication) context.getApplicationContext()).inject(this);
    }

    public void setList(List<Weather5DaysEntity.WeatherList> list) {
        this.list = list;
    }

    @Override
    protected View getView(int position) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_day_of_a_wether, null, false);
        ButterKnife.inject(this, v);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        param.weight = 1;
        setLayout(position, v);
        final int finalI = position;
        setDefaultTabLayoutParams(new LinearLayout.LayoutParams((int) getResources().getDimension(R.dimen.week_item_width), LinearLayout.LayoutParams.WRAP_CONTENT));
        setTabPaddingLeftRight((int) getResources().getDimension(R.dimen.week_item_padding_right_left));
        return v;
    }

    @Override
    protected void updateTabStyles() {
        //overide do Nothing
//        super.updateTabStyles();
    }

    private void setLayout(int position, View v) {
        Weather5DaysEntity.WeatherList weather = list.get(position);
        tempMax.setText(WeatherUtil.getKelvinTempString(weather.temp.max)+ " (max)");
        tempMin.setText(WeatherUtil.getKelvinTempString(weather.temp.min)+ " (min)");
        date.setText(WeatherUtil.getDateStringFromUnixTime(weather.dt));
        picasso.load(WeatherApiAccessor.weatherIconUrl(weather.weather.get(0).icon))
                .into(imageWeather);
    }
}
