package weeklyweather.sasakinao.co.jp.src.weeklyweather.top.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.InjectView;
import weeklyweather.sasakinao.co.jp.src.common.util.Logger;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.BusHolder;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.Weather3HourEntity;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.Weather5DaysEntity;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.top.component.WeekTabStrip;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.top.component.WeekWeatherPagerAdapter;
import weeklyweather.sasakinao.co.jp.weeklyweather.R;

public class WeekWeatherFragment extends Fragment {

    private Weather5DaysEntity weather5DaysEntity;
    private Weather3HourEntity weather3Hour;

    @InjectView(R.id.pager)
    ViewPager viewPager;

    @InjectView(R.id.pager_tab_strip)
    WeekTabStrip pagerTabStrip;

    @Override
    public void onResume() {
        super.onResume();
        BusHolder.get().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusHolder.get().unregister(this);
    }

    public static WeekWeatherFragment getInstance(Weather5DaysEntity weather5DaysEntity, Weather3HourEntity weather3HourEntity) {
        WeekWeatherFragment fragment = new WeekWeatherFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("5days_weather", weather5DaysEntity);
        bundle.putSerializable("3hour_weather", weather3HourEntity);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_week_weather, container, false);
        ButterKnife.inject(this, layout);

        weather5DaysEntity = (Weather5DaysEntity) getArguments().getSerializable("5days_weather");
        weather3Hour = (Weather3HourEntity) getArguments().getSerializable("3hour_weather");

        Logger.v(this, "create", "weather5DaysEntity.list.size():" + weather5DaysEntity.list.size());
        viewPager.setAdapter(new WeekWeatherPagerAdapter(((FragmentActivity) getActivity()).getSupportFragmentManager(), weather3Hour, weather5DaysEntity));
        pagerTabStrip.setList(weather5DaysEntity.list);
        pagerTabStrip.setViewPager(viewPager);
        pagerTabStrip.setUnderlineHeight(0);

        return layout;
    }

}
