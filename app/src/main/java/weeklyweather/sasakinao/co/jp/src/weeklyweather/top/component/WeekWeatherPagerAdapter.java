package weeklyweather.sasakinao.co.jp.src.weeklyweather.top.component;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import java.text.SimpleDateFormat;
import java.util.Date;

import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.Weather3HourEntity;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.Weather5DaysEntity;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.top.ui.OneDayWeatherFragment;

public class WeekWeatherPagerAdapter extends FragmentStatePagerAdapter {

    private final Weather3HourEntity weatherEntity;
    private final Weather5DaysEntity weather5DaysEntity;

    private SparseArray<OneDayWeatherFragment> fragments;

    public WeekWeatherPagerAdapter(FragmentManager childFragmentManager, Weather3HourEntity weather3Hour, Weather5DaysEntity weather5DaysEntity) {
        super(childFragmentManager);
        weatherEntity = weather3Hour;
        this.weather5DaysEntity = weather5DaysEntity;
        fragments = new SparseArray<OneDayWeatherFragment>();
        for (int i = 0; i < weather5DaysEntity.cnt; i++) {
            fragments.put(i, OneDayWeatherFragment.getInstance(i, weather3Hour, weather5DaysEntity.list.get(i).dt));
        }
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy年MM月dd日");
        String str2 = sdf1.format(new Date(weather5DaysEntity.list.get(position).dt * 1000));
        return str2;
    }


}