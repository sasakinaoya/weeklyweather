package weeklyweather.sasakinao.co.jp.src.weeklyweather.top.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import butterknife.ButterKnife;
import butterknife.InjectView;
import weeklyweather.sasakinao.co.jp.src.WeeklyWeatherApplication;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.BusHolder;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.Weather3HourEntity;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.top.component.GraphLineView;
import weeklyweather.sasakinao.co.jp.src.weeklyweather.top.component.WeekWeatherLayoutView;
import weeklyweather.sasakinao.co.jp.weeklyweather.R;

public class OneDayWeatherFragment extends Fragment {

    @InjectView(R.id.graph_view)
    GraphLineView graphView;

    private Weather3HourEntity weather;


    @Override
    public void onResume() {
        super.onResume();
        BusHolder.get().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusHolder.get().unregister(this);
    }


    public static OneDayWeatherFragment getInstance(int i, Weather3HourEntity weatherList, long dt) {
        OneDayWeatherFragment fragment = new OneDayWeatherFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("fragment_index", i);
        bundle.putSerializable("fragment_weather", weatherList);
        bundle.putLong("dt",dt);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_one_day_weather, container, false);
        ButterKnife.inject(this, layout);
        ((WeeklyWeatherApplication) getActivity().getApplication()).inject(this);
        int index = getArguments().getInt("fragment_index");
        long dt = getArguments().getLong("dt");
        weather = (Weather3HourEntity) getArguments().getSerializable("fragment_weather");

        graphView.init(weather);
        graphView.startGraphAnimation(dt);

        return layout;
    }

    @Subscribe
    public void onFetchedWeather(WeekWeatherLayoutView.Event event) {
//        Logger.i(this,"onFetchedWeather","event:"+event);
        graphView.startGraphAnimation(event.weatherList.dt);
    }


}
