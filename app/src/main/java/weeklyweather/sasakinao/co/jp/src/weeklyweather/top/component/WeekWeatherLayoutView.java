package weeklyweather.sasakinao.co.jp.src.weeklyweather.top.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import weeklyweather.sasakinao.co.jp.src.WeeklyWeatherApplication;
import weeklyweather.sasakinao.co.jp.src.common.util.Logger;
import weeklyweather.sasakinao.co.jp.src.common.util.WeatherUtil;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.BusHolder;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.WeatherApiAccessor;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.entity.Weather5DaysEntity;
import weeklyweather.sasakinao.co.jp.weeklyweather.R;

public class WeekWeatherLayoutView extends FrameLayout {

    @Inject
    Picasso picasso;

    @InjectView(R.id.date)
    TextView date;
    @InjectView(R.id.temp_max)
    TextView tempMax;

    @InjectView(R.id.temp_min)
    TextView tempMin;

    @InjectView(R.id.image_weather)
    ImageView imageWeather;

    private LinearLayout parentLayout;
    private Context context;
    private List<Weather5DaysEntity.WeatherList> list;
    private int selectedPosition;

    public class Event{

        public final Weather5DaysEntity.WeatherList weatherList;

        public Event(Weather5DaysEntity.WeatherList weatherList) {
            this.weatherList= weatherList;
        }
    }


    public WeekWeatherLayoutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ((WeeklyWeatherApplication) context.getApplicationContext()).inject(this);

        this.context = context;
        parentLayout = new LinearLayout(context);
        parentLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        parentLayout.setOrientation(LinearLayout.HORIZONTAL);

        addView(parentLayout);
    }

    private void notifyDataSetChanged() {
        Logger.i(this, "notifidata", "call");
        parentLayout.removeAllViews();
        for (int i = 0; i < list.size()-1; i++) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.item_day_of_a_wether, null, false);
            ButterKnife.inject(this, v);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            param.weight = 1;
            setLayout(i, v);
            final int finalI = i;
            v.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(selectedPosition != finalI){
                        selectedPosition = finalI;
                        changeBackGround();
                        selectedWeather();
                    }
                }
            });

            parentLayout.addView(v,param);
        }
        changeBackGround();
        selectedWeather();
    }

    private void changeBackGround() {
        for(int i = 0; i < parentLayout.getChildCount(); i++){
            View v = parentLayout.getChildAt(i);
            if(i == selectedPosition){
                v.setBackgroundResource(R.drawable.ic_launcher);
            } else {
                v.setBackgroundResource(0);
            }
        }
    }

    public void selectedWeather() {
        BusHolder.get().post(new Event(list.get(selectedPosition)));
    }

    public void setLayout(int i, View v) {
        Weather5DaysEntity.WeatherList weather = list.get(i);
        tempMax.setText(WeatherUtil.getKelvinTempString(weather.temp.max));
        tempMin.setText(WeatherUtil.getKelvinTempString(weather.temp.min));
        date.setText(WeatherUtil.getDateStringFromUnixTime(weather.dt));
        picasso.load(WeatherApiAccessor.weatherIconUrl(weather.weather.get(0).icon))
                .into(imageWeather);

    }

    public void addAll(List<Weather5DaysEntity.WeatherList> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}
