package weeklyweather.sasakinao.co.jp.src.model.location;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import weeklyweather.sasakinao.co.jp.src.common.util.Logger;
import weeklyweather.sasakinao.co.jp.src.model.weatherapi.BusHolder;

//TODO タイムアウト設定
public class LocationGetter implements LocationListener {

    Context context;
    // LocationManagerを取得
    LocationManager mLocationManager;

    private boolean isFetching;

    public class Event{
        public Location location;

        public Event(Location loation) {
            this.location = loation;
        }

        public boolean isVaild() {
            return location != null;
        }
    }

    public LocationGetter(Context context) {
        this.context = context;
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public void fetchLocation(){


        // Criteriaオブジェクトを生成
        Criteria criteria = new Criteria();

        // Accuracyを指定(低精度)
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);

        // PowerRequirementを指定(低消費電力)
        criteria.setPowerRequirement(Criteria.POWER_LOW);

        // ロケーションプロバイダの取得
        String provider = mLocationManager.getBestProvider(criteria, true);

        // LocationListenerを登録
        mLocationManager.requestLocationUpdates(provider, 0, 0, this);
        isFetching = true;
    }

    @Override
    public void onLocationChanged(Location location) {
        isFetching = false;
        mLocationManager.removeUpdates(this);
        BusHolder.get().post(new Event(location));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Logger.i(this, "onStatusChanged", "status:"+status);
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        isFetching = true;
        mLocationManager.removeUpdates(this);
        BusHolder.get().post(new Event(null));
    }

    public boolean isFetching() {
        return isFetching;
    }

    public void setFetching(boolean isFetching) {
        this.isFetching = isFetching;
    }
}
